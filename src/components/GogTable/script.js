import GogCard from "@/components/GogCard";
import Button from "primevue/button/sfc";
import SelectButton from "primevue/selectbutton/sfc";
import InputNumber from "primevue/inputnumber/sfc";
import Toolbar from "primevue/toolbar/sfc";

export default {
  data() {
    return {
      card: false,
      cardOptions: [
        {
          class: "pi pi-bars",
          card: false
        },
        {
          class: "pi pi-th-large",
          card: true
        }
      ],
      page: 1,
      products: [],
      totalPages: "-"
    };
  },
  methods: {
    fetch() {
      clearTimeout(this.$options.timeout);
      this.$options.timeout = setTimeout(() => {
        fetch(
          "https://cors-anywhere.herokuapp.com/" +
            `https://www.gog.com/games/ajax/filtered?hide=dlc&mediaType=game&page=${this.page}&sort=popularity`
        )
          .then(value => value.json())
          .then(value => {
            this.totalPages = value.totalPages;
            Promise.all(
              value.products.map(currentValue =>
                fetch(
                  `https://api.gog.com/v2/games/${currentValue.id}`
                ).then(value => value.json())
              )
            ).then(value => {
              this.products = value;
            });
          });
      }, 0);
    },
    swap(drag, drop) {
      const dragElement = this.products.find(
        element => element._embedded.product.id === drag
      );
      const dragIndex = this.products.findIndex(
        element => element._embedded.product.id === drag
      );
      const dropIndex = this.products.findIndex(
        element => element._embedded.product.id === drop
      );
      this.products.splice(dragIndex, 1);
      this.products.splice(dropIndex, 0, dragElement);
    }
  },
  watch: {
    page() {
      this.fetch();
    }
  },
  mounted() {
    this.fetch();
  },
  components: {
    GogCard,
    Button,
    InputNumber,
    SelectButton,
    Toolbar
  },
  name: "GogTable"
};
