export default {
  data() {
    return {
      columns: ["id", "title", "globalReleaseDate", "gogReleaseDate"],
      show: false
    };
  },
  props: {
    card: Boolean,
    product: Object
  },
  methods: {
    click() {
      this.show = !this.show;
    },
    dragleave() {
      this.$el.classList.remove("drop");
    },
    dragover(event) {
      event.preventDefault();
      if (
        Number(event.dataTransfer.getData("text/plain")) !==
        this.product._embedded.product.id
      ) {
        this.$el.classList.add("drop");
      }
    },
    dragstart(event) {
      event.dataTransfer.setData(
        "text/plain",
        this.product._embedded.product.id
      );
      event.dataTransfer.setDragImage(
        event.target,
        event.offsetX,
        event.offsetY
      );
    },
    drop(event) {
      event.preventDefault();
      const dragId = Number(event.dataTransfer.getData("text/plain"));
      if (dragId !== this.product._embedded.product.id) {
        this.$el.classList.remove("drop");
        this.$emit("swap", dragId, this.product._embedded.product.id);
      }
    }
  },
  name: "GogCard"
};
